PROJECT_PWD := `pwd`

MARIADB_VERSION := latest
#MARIADB_VERSION := 3.1

MARIADB_IMG := mariadb:$(MARIADB_VERSION)

CONTAINER_MARIADB_NAME := mariadb-v$(MARIADB_VERSION)
#CONTAINER_MARIADB_VOLUME := $(PROJECT_PWD)/ucs-mariadb-db/:/var/lib/cassandra

DB_PASSWORD := Default

TEST_NAME := linked-table
TESTS_PATH := $(PROJECT_PWD)/tests/$(TEST_NAME)

TESTS_MODEL_PATH := $(TESTS_PATH)/
TESTS_MODEL_CQL := model.sql

TESTS_FIXTURE_PATH := $(TESTS_PATH)/
TESTS_FIXTURE_CQL := fixtures.sql

TESTS_DROP_PATH := $(TESTS_PATH)/
TESTS_DROP_CQL := drop-model.sql

TEST_PATH := $(TESTS_PATH)/
TEST_CQL := test.sql

all: docker-mariadb-create

##########
# DOCKER #
##########

# env
#####

DOCKER_ENV =\
	MYSQL_ROOT_PASSWORD=$(DB_PASSWORD)

# variable
##########

DOCKER := $(DOCKER_ENV) docker

CONTAINER_MARIADB_IP := 172.17.0.2

# tests
#######

docker-mariadb-tests-set-models:
	$(DOCKER) cp $(TESTS_MODEL_PATH)$(TESTS_MODEL_CQL) $(CONTAINER_MARIADB_NAME):/$(TESTS_MODEL_CQL)
	$(DOCKER) exec $(CONTAINER_MARIADB_NAME) /bin/sh -c 'exec mysql -u root --password=$(DB_PASSWORD) < $(TESTS_MODEL_CQL)'

docker-mariadb-tests-set-fixture:
	$(DOCKER) cp $(TESTS_FIXTURE_PATH)$(TESTS_FIXTURE_CQL) $(CONTAINER_MARIADB_NAME):/$(TESTS_FIXTURE_CQL)
	$(DOCKER) exec $(CONTAINER_MARIADB_NAME) /bin/sh -c 'exec mysql -u root --password=$(DB_PASSWORD) < $(TESTS_FIXTURE_CQL)'

docker-mariadb-tests-drop-models:
	$(DOCKER) cp $(TESTS_DROP_PATH)$(TESTS_DROP_CQL) $(CONTAINER_MARIADB_NAME):/$(TESTS_DROP_CQL)
	$(DOCKER) exec $(CONTAINER_MARIADB_NAME) /bin/sh -c 'exec mysql -u root --password=$(DB_PASSWORD) < $(TESTS_DROP_CQL)'

docker-mariadb-tests-run-test:
	$(DOCKER) cp $(TEST_PATH)$(TEST_CQL) $(CONTAINER_MARIADB_NAME):/$(TEST_CQL)
	$(DOCKER) exec $(CONTAINER_MARIADB_NAME) /bin/sh -c 'exec mysql -u root --password=$(DB_PASSWORD) < $(TEST_CQL)'

docker-mariadb-tests-build: docker-mariadb-tests-drop-models docker-mariadb-tests-set-models docker-mariadb-tests-set-fixture

docker-mariadb-tests: docker-mariadb-tests-drop-models docker-mariadb-tests-set-models docker-mariadb-tests-set-fixture docker-mariadb-tests-run-test docker-mariadb-tests-drop-models

docker-mariadb-tests-custom:
	$(DOCKER) cp $(DOCKER_TESTS_CQL) $(CONTAINER_MARIADB_NAME):/$(DOCKER_TESTS_CQL)
	$(DOCKER) exec $(CONTAINER_MARIADB_NAME) /bin/sh -c 'exec mysql -f $(DOCKER_TESTS_CQL) $(CONTAINER_MARIADB_IP)'


# container
###########

mariadb-ip:
	$(DOCKER) inspect --format='{{ .NetworkSettings.IPAddress }}' $(CONTAINER_MARIADB_NAME)

docker-mariadb-create:
	$(DOCKER) run --name $(CONTAINER_MARIADB_NAME) -e MYSQL_ROOT_PASSWORD=$(DB_PASSWORD) -m 2g -d $(MARIADB_IMG)

docker-mariadb-start:
	$(DOCKER) start $(CONTAINER_MARIADB_NAME)

docker-mariadb-connect:
	$(DOCKER) run -it --link $(CONTAINER_MARIADB_NAME) --rm $(MARIADB_IMG) sh -c 'exec mysql $(CONTAINER_MARIADB_IP)'

docker-mariadb-cp-file-to:
	$(DOCKER) cp $(fileToCopyPath)$(fileToCopyName) $(CONTAINER_MARIADB_NAME):/$(fileToCopyName)

docker-mariadb-bash:
	$(DOCKER) exec -it $(CONTAINER_MARIADB_NAME) bash

docker-mariadb-stop:
	$(DOCKER) stop `docker ps | grep $(CONTAINER_MARIADB_NAME) | cut -d ' ' -f1`

docker-mariadb-stop-rm:
	$(DOCKER) stop `docker ps | grep $(CONTAINER_MARIADB_NAME) | cut -d ' ' -f1` | xargs docker rm

###########
# /DOCKER #
###########
