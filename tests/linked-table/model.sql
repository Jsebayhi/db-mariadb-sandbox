CREATE DATABASE IF NOT EXISTS linked_table;


CREATE TABLE IF NOT EXISTS linked_table.resources (
	id INT NOT NULL AUTO_INCREMENT,
	stuff varchar (128),
	PRIMARY KEY (id)
) COLLATE='utf8_bin';

CREATE TABLE IF NOT EXISTS linked_table.simple_table (
	id INT NOT NULL AUTO_INCREMENT,
	description varchar (128),
	resource int(11),
	PRIMARY KEY (id),
	FOREIGN KEY (resource) REFERENCES linked_table.resources(id)
) COLLATE='utf8_bin';
